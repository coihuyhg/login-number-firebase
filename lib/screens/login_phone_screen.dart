import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

import 'verified_screen.dart';

class LoginPhoneScreen extends StatefulWidget {
  const LoginPhoneScreen({Key? key}) : super(key: key);

  static String receivedID = '';

  @override
  State<LoginPhoneScreen> createState() => _LoginPhoneScreenState();
}

class _LoginPhoneScreenState extends State<LoginPhoneScreen> {
  final TextEditingController phoneController = TextEditingController();
  TextEditingController countryCode = TextEditingController();

  String userNumber = '';
  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    countryCode.text = "+84";
  }

  void verifyUserPhoneNumber() {
    auth.verifyPhoneNumber(
      phoneNumber: countryCode.text + userNumber,
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int? resendToken) {
        LoginPhoneScreen.receivedID = verificationId;
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => const VerifiedScreen()));
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 42, right: 42, top: 73),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Phone", style: TextStyle(fontSize: 16)),
            const SizedBox(height: 5),
            // IntlPhoneField(
            //   initialCountryCode: 'VN',
            //   controller: phoneController,
            //   decoration: const InputDecoration(
            //       border: OutlineInputBorder(
            //           borderRadius: BorderRadius.all(Radius.circular(5))),
            //       hintText: "Enter your phone"),
            //   onChanged: (value) {
            //     userNumber = value.completeNumber;
            //   },
            // ),
            TextFormField(
              keyboardType: TextInputType.phone,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  hintText: "Enter your phone"),
              onChanged: (value) {
                userNumber = value;
              },
            ),
            const SizedBox(height: 60),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 55,
              child: ElevatedButton(
                onPressed: verifyUserPhoneNumber,
                child: Text("sign in".toUpperCase(),
                    style: const TextStyle(fontSize: 14)),
              ),
            )
          ],
        ),
      ),
    );
  }
}
