import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'login_phone_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  FirebaseAuth auth = FirebaseAuth.instance;

  void signOutHome() async {
    await auth.signOut();
    navigatorHome();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 42, right: 42, top: 73),
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 55,
          child: ElevatedButton(
            onPressed: signOutHome,
            child: Text("sign out".toUpperCase(),
                style: const TextStyle(fontSize: 14)),
          ),
        ),
      ),
    );
  }

  navigatorHome() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => const LoginPhoneScreen()));
  }
}