import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:login_phone_firebase/screens/home_screen.dart';
import 'package:login_phone_firebase/screens/login_phone_screen.dart';

class VerifiedScreen extends StatefulWidget {
  const VerifiedScreen({Key? key}) : super(key: key);

  @override
  State<VerifiedScreen> createState() => _VerifiedScreenState();
}

class _VerifiedScreenState extends State<VerifiedScreen> {
  final TextEditingController otpController = TextEditingController();
  FirebaseAuth auth = FirebaseAuth.instance;

  Future<void> verifyOTPCode() async {
    try {
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: LoginPhoneScreen.receivedID,
        smsCode: otpController.text,
      );
      await auth
          .signInWithCredential(credential)
          .then((value) => print('User Login In Successful'));
      navigatorHome();
      showDialogHome();
    } catch (e) {
      print(e.toString());
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text("Sai Otp")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 42, right: 42, top: 73),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("otp".toUpperCase(), style: const TextStyle(fontSize: 16)),
            const SizedBox(height: 5),
            TextFormField(
              onChanged: (value) {
                otpController.text = value;
              },
              keyboardType: TextInputType.phone,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  hintText: "Enter your code"),
            ),
            const SizedBox(height: 60),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 55,
              child: ElevatedButton(
                  onPressed: verifyOTPCode,
                  child: const Text("Confirm OTP",
                      style: TextStyle(fontSize: 14))),
            )
          ],
        ),
      ),
    );
  }

  navigatorHome() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const HomeScreen()));
  }


  void showDialogHome() {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(21),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Title",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 25),
                const Text(
                  "Body",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                ),

                const SizedBox(height: 60),
                SizedBox(
                  width: 105,
                  height: 40,
                  child: ElevatedButton(
                    style: const ButtonStyle(
                      backgroundColor:
                      MaterialStatePropertyAll<Color>(Color(0xFFD9D9D9)),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      "Cancel",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF6B6B6B),
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
