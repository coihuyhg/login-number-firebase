import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:login_phone_firebase/firebase_options.dart';
import 'package:login_phone_firebase/screens/login_phone_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    name: 'otp_phone',
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'demo_login_otp',
      home: LoginPhoneScreen(),
    );
  }
}